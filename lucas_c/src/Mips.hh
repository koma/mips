/**
 * @file Mips.hh
 * @author lucas_c
 */

#ifndef MIPS_HH_
# define MIPS_HH_

# include <list>

# include "Instruction.hh"
# include "Registers.hh"
# include "Vmem.hh"

class Mips
{
public:
    Mips();

    bool    initSimu(const char* file);

private:
    void    initRegisters();

    // Fetch
    // Decode
    // Execute

protected:
    //void dumpRegs();

    //void step();

    //void run();

private:
    std::list<Instruction>  instrs;
    registers_t             registers;
    vmem_t*                 vmem;       // Virtual memory
    int                     pc;         // program counter
    int                     limit;      // End of instructions
};

#endif /* !MIPS_HH_ */
