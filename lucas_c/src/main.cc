#include <unistd.h>
#include <cstdint>
#include <iostream>

#include "Mips.hh"

// Usage of the program
static void usage(char *name)
{
    std::cout << name << " usage:"  << std::endl
        << "\t-d data_stream_file: [optional] load .data with "
        << "contents of file\n" << std::endl
        << "\t-v: very verbose single-click CPU (echo every stage, pause after"
        << "each cycle)" << std::endl;

    return;
}


int main(int argc, char** argv)
{
    bool    verbose = false;
    int32_t ch = 0;
    Mips    mips;

    // Data segment
    // Text segment

    if (argc == 1)
    {
        usage(*argv);
        exit(EXIT_FAILURE);
    }

    while ((ch = getopt(argc, argv, "dv")) != -1)
    {
        switch (ch)
        {
            case 'd':
                {
                    if (argc != 3 && !mips.initSimu(argv[2]))
                    {
                        std::cerr << *argv
                            << " : error during initialization" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
                break;
            case 'v':
                verbose = true;
                break;
            default:
                usage(*argv);
                exit(EXIT_FAILURE);
                break;
        }
    }

    std::cout << *argv << ": Starting CPU..." << std::endl;
    //run_cpu(&mem, verb);
    std::cout << *argv << ": CPU Finished" << std::endl;

    return EXIT_SUCCESS;
}
