#include <stdlib.h>
#include <string.h>

#include "Vmem.hh"

#define BYTE_TO_WORD(Vm, P) \
    *((DATA_WORD*)(Vm->address.bytemem + P))

vmem_t*     vmemCreate()
{
    vmem_t* vm = NULL;

    if ((vm = (vmem_t*)malloc(sizeof (vmem_t))))
    {
        vm->address.mem = (DATA_WORD*)malloc(VMEM_SIZE * sizeof (DATA_WORD));
        if (!vm->address.mem)
        {
            free(vm);
            return NULL;
        }
    }

    return vm;
}

void    vmemDestroy(vmem_t* vm)
{
    if (!vm)
        return;
    free(vm->address.mem);
    free(vm);

    return;
}

void    vmemSetWord(vmem_t* vm, int pos, DATA_WORD value)
{
    if (pos >= 0 && pos < VMEM_SIZE - 1)
        BYTE_TO_WORD(vm, pos) = value;
}

void    vmemSetNWord(vmem_t* vm, int pos, DATA_WORD* value, int count)
{
    if (pos >= 0 && pos + count < VMEM_SIZE - 1)
        memcpy(&vm->address.bytemem[pos],
               value, count * sizeof(DATA_WORD));
}

DATA_WORD   vmemGetWord(vmem_t* vm, int pos)
{
    return (pos >= 0 && pos < VMEM_SIZE - 1 ?
            BYTE_TO_WORD(vm, pos) : 0);
}
