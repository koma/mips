#include "Mips.hh"

#include <iostream>
#include <fstream>
#include <cstring>

Mips::Mips()
{
    initRegisters();
    vmem = vmemCreate();
    pc = 0;
    limit = 0;
}

bool Mips::initSimu(const char* file)
{
    std::ifstream input(file, std::ios::binary);

    if (!(input.good() && input.is_open()))
    {
        std::cerr << file << " does not exist" << std::endl;
        return false;
    }

    Instruction instr;

    std::cout << "parsing..." << std::endl;
    while (!input.eof())
    {
        input.read((char*)&instr, sizeof (Instruction));
        instrs.push_back(instr);
    }

    input.close();

    return true;
}

void Mips::initRegisters()
{
    for (int i = 0; i < NUM_REGISTERS; ++i)
        registers.insert(std::make_pair(i, 0));

    return;
}
