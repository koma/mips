/**
 * @file Instruction.hh
 * @author lucas_c
 */
#ifndef INSTRUCTION_HH_
# define INSTRUCTION_HH_

# include <cstdint>

typedef uint8_t byte;

typedef struct {
  byte opcode : 6;
  union {
    struct {
      byte rs : 5;
      byte rt : 5;
      uint16_t imm;
    } i;
    struct {
      byte rs : 5;
      byte rt : 5;
      byte rd : 5;
      byte shift : 5;
      byte funct : 6;
    } r;
    struct {
      uint32_t address : 24;
    } j;
  } field;
}       Instruction;

#endif /* !INSTRUCTION_HH_ */
