/**
 * @file Vmem.hh
 * @author lucas_c & ly-tan_g
 */
#ifndef VMEM_H_
# define VMEM_H_

# include "Types.hh"

# define VMEM_SIZE (2 << 16)

typedef struct vmem
{
    union
    {
        DATA_WORD*  mem;        // Word addressed
        char*       bytemem;    // Byte addressed
    } address;
}   vmem_t;

vmem_t*     vmemCreate();

void        vmemDestroy(vmem_t* vm);

void        vmemSetWord(vmem_t* vm, int pos, DATA_WORD value);

void        vmemSetNWord(vmem_t* vm, int pos, DATA_WORD* value, int count);

DATA_WORD   vmemGetWord(vmem_t* vm, int pos);

#endif /* !VMEM_H_ */
