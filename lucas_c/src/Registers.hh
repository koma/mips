/**
 * @file Registers.hh
 * @author lucas_c & ly-tan_g
 */
#ifndef REGISTERS_HH_
# define REGISTERS_HH_

# include <tbb/concurrent_hash_map.h>
# include "Types.hh"

# define NUM_REGISTERS  32

typedef tbb::concurrent_hash_map<int, DATA_WORD> registers_t;

#endif /* !REGISTERS_HH_ */
