.text

.globl main

main:
    addi $t0, $zero, 3
    addi $t1, $zero, 2
    add $t2, $t0, $t1
    seq $t3, $t0, $t1
    seq $t4, $t0, $t0
    jr $ra
